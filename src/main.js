import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import DemoActions from "@/pages/DemoActions";
import Mailchimp from "@/pages/Mailchimp";
import AWeberIndex from "@/components/AWeber/index";
import AWeberLogin from "@/components/AWeber/AWeberLogin";
import ConvertKitIndex from "@/components/ConvertKit/index";

Vue.config.productionTip = false


Vue.use(Buefy)
Vue.use(VueRouter)


const routes = [
    {name: 'demo-actions', path: '/demo-actions', component: DemoActions},
    {name: 'mailchimp', path: '/mailchimp', component: Mailchimp},
    {name: 'mailchimp-callback', path: '/oauth/mailchimp/callback', component: Mailchimp},
    {name: 'aweber', path: '/aweber', component: AWeberIndex},
    {name: 'aweber-callback', path: '/oauth/aweber/callback', component: AWeberLogin},
    {name: 'convertkit', path: '/convertkit', component: ConvertKitIndex},
]

const router = new VueRouter({
    mode: 'history',
    routes: routes
})


new Vue({el: '#app', router, render: h => h(App)})
